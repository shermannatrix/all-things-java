/**
 * The Displayer class displays text on the computer screen.
 *
 * @author  Sherman
 * @version 1.0
 * @see     java.lang.System
 */
public class Displayer {
    public static void main(String[] args) {
        System.out.println("I love Java!");
    }
}
