package Listing44_CharDemo;

public class CharDemo {
    public static void main(String[] args) {
        char myLittleChar = 'b';
        char myBigChar = Character.toUpperCase(myLittleChar);

        System.out.println(myBigChar);
    }
}
