package BasicClasses.accounts;

import static java.lang.System.out;

public class Account {
	public String name;
	public String address;
	public double balance;

	// From Listing 7-3
	public void display() {
		out.print(name);
        out.print(" (");
        out.print(address);
        out.print(") has $");
        out.print(balance);
        out.println();
	}

	// From Listing 7-5
	public double getInterest(double percentageRate) {
		return balance * percentageRate / 100.00;
	}
}